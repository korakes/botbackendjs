'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BootsSchema extends Schema {
  up () {
    this.dropIfExists('boots')
    this.create('boots', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down () {
    this.drop('boots')
  }
}

module.exports = BootsSchema
