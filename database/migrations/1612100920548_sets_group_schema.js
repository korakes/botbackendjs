'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SetsGroupSchema extends Schema {
  up () {
    this.dropIfExists('s_groups')
    this.create('s_groups', (table) => {
      table.increments()
      table.integer('weapon').unsigned().references('id').inTable('weapons')
      table.integer('offhand').unsigned().references('id').inTable('offhands').nullable()
      table.integer('helmet').unsigned().references('id').inTable('helmets')
      table.integer('armor').unsigned().references('id').inTable('armors')
      table.integer('boots').unsigned().references('id').inTable('boots')
      table.integer('cape').unsigned().references('id').inTable('capes')
      table.string('slug', 32)
      table.string('permission_rank_id', 32)
      table.enum('type', ['Tank', 'Rdps', 'Mdps', 'Healer', 'Support'])
      table.timestamps()
    })
  }
  down () {
    this.drop('sets_groups')
  }
}

module.exports = SetsGroupSchema
