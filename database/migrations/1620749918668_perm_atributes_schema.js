'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermAtributesSchema extends Schema {
  up () {
    this.create('perm_attributes', (table) => {
      table.increments()
      table.string('name', 32).notNullable()
      table.enum('type', ['canSee', 'canDo']).notNullable()
      table.bool('value').default(0).notNullable()
      table.integer('rank').unsigned().references('id').inTable('permissions')
      table.timestamps()
    })
  }

  down () {
    this.drop('perm_atributes')
  }
}

module.exports = PermAtributesSchema
