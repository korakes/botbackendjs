'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecomendationsSchema extends Schema {
  up () {
    this.create('recomendations', (table) => {
      table.increments()
      table.string('name', 255)
      table.string('referrer', 255)
      table.timestamps()
    })
  }

  down () {
    this.drop('recomendations')
  }
}

module.exports = RecomendationsSchema
