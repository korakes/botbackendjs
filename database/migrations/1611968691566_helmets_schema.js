'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HelmetsSchema extends Schema {
  up () {
    this.dropIfExists('helmets')
    this.create('helmets', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down () {
    this.drop('helmets')
  }
}

module.exports = HelmetsSchema
