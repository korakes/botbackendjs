'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RegearsSchema extends Schema {
  up () {
    this.create('regears', (table) => {
      table.increments()
      table.integer('user').unsigned().references('id').inTable('members')
      table.integer('zestaw').unsigned().references('id').inTable('s_groups')
      table.string('url', 255)
      table.bool('isDone').default(0)
      table.bool('isRejected').default(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('regears')
  }
}

module.exports = RegearsSchema
