'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FineSchema extends Schema {
  up () {
    this.create('fines', (table) => {
      table.increments()
      table.string('nick')
      table.string('value')
      table.string('reason')
      table.boolean('status').notNullable().defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('fines')
  }
}

module.exports = FineSchema
