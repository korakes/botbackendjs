'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmorsSchema extends Schema {
  up() {
    this.dropIfExists('armors')
    this.create('armors', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down() {
    this.drop('armors')
  }
}

module.exports = ArmorsSchema
