'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RdayDeclarationsSchema extends Schema {
  up () {
    this.dropIfExists('rday_declarations')
    this.create('rday_declarations', (table) => {
      table.increments()
      table.integer('user').unsigned().references('id').inTable('members')
      table.integer('zestaw').unsigned().references('id').inTable('s_groups')
      table.integer('rday').unsigned().references('id').inTable('rdays')
      table.timestamps()
    })
  }

  down () {
    this.drop('rday_declarations')
  }
}

module.exports = RdayDeclarationsSchema
