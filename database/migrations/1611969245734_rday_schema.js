'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RdaySchema extends Schema {
  up () {
    this.dropIfExists('rdays')
    this.create('rdays', (table) => {
      table.increments()
      table.boolean('status').notNullable().defaultTo(0)
      table.string('name', 32)
      table.timestamps()
    })
  }

  down () {
    this.drop('rdays')
  }
}

module.exports = RdaySchema
