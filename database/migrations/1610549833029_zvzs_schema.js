'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZvzsSchema extends Schema {
  up () {
    this.create('zvzs', (table) => {
      table.increments()
      table.string('note', 32)
      table.timestamps()
    })
  }

  down () {
    this.drop('zvzs')
  }
}

module.exports = ZvzsSchema