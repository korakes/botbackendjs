'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WeaponsSchema extends Schema {
  up() {
    this.dropIfExists('weapons')
    this.create('weapons', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down() {
    this.drop('weapons')
  }
}

module.exports = WeaponsSchema
