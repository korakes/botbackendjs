'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CapesSchema extends Schema {
  up() {
    this.dropIfExists('capes')
    this.create('capes', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down() {
    this.drop('capes')
  }
}

module.exports = CapesSchema
