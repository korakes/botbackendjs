'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RanksSchema extends Schema {
  up () {
    this.create('ranks', (table) => {
      table.increments()
      table.integer('user').unsigned().references('id').inTable('users')
      table.integer('rank').unsigned().references('id').inTable('permissions')
      table.timestamps()
    })
  }

  down () {
    this.drop('ranks')
  }
}

module.exports = RanksSchema
