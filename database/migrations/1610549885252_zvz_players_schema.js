'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZvzPlayersSchema extends Schema {
  up () {
    this.create('zvz_players', (table) => {
      table.increments()
      table.integer('zvz_id').unsigned().references('id').inTable('zvzs')
      table.integer('user').unsigned().references('id').inTable('members')
      table.timestamps()
    })
  }

  down () {
    this.drop('zvz_players')
  }
}

module.exports = ZvzPlayersSchema
