'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BlacklistsSchema extends Schema {
  up () {
    this.create('blacklists', (table) => {
      table.increments()
      table.string('nick')
      table.string('discordid', 64)
      table.string('reason', 255)
      table.timestamps()
    })
  }

  down () {
    this.drop('blacklists')
  }
}

module.exports = BlacklistsSchema
