'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MembersSchema extends Schema {
  up () {
    this.create('members', (table) => {
      table.increments()
      table.string('nick').notNullable().unique()
      table.string('ing_key').notNullable().unique()
      table.string('discord_id').notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('members')
  }
}

module.exports = MembersSchema
