'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DodanieIsobligatedDoMembersSchema extends Schema {
  up () {
    this.table('members', (table) => {
      table.boolean('isObligated').defaultTo(0).after('discord_id')
      // alter table
    })
  }

  down () {
    this.table('members', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DodanieIsobligatedDoMembersSchema
