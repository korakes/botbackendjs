'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OffhandsSchema extends Schema {
  up() {
    this.dropIfExists('offhands')
    this.create('offhands', (table) => {
      table.increments()
      table.string('name')
      table.string('item_name', 64)
      table.timestamps()
    })
  }

  down() {
    this.drop('offhands')
  }
}

module.exports = OffhandsSchema
