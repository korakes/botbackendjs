'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Weapon', (faker, i, data) => {
    return {
        name:
            ['Camlann Mace', 'Grovekeeper', 'Soulscythe', 'Galantine pair', 'Halberd',
                'Brimstone Staff', 'Permafrost', 'Icicle Staff', 'Malevolent Locus', 'Occult Staff',
                'Fallen Staff', 'Rampant Staff', 'Damnation Staff', 'Weeping Reapeter', 'Siegebow',
                'Wailing Bow', 'Longbow', 'Great Frost', 'Bloodletter', 'Dual Swords', 'Bear Paws',
                'Morning Star', 'Heavy Mace', 'Great Holy Staff', 'Blight Staff', 'Carrioncaller',
                'Infernal Scythe', 'Enigmatic Staff', 'Energy Shaper', 'Daybreker', 'Evensong',
                'Wild Staff', 'Mistpiercer', 'Clarent', 'wlasny'][i],
        item_name:
            ['T8_2H_MACE_MORGANA', 'T8_2H_RAM_KEEPER', 'T8_2H_TWINSCYTHE_HELL', 'T8_2H_DUALSCIMITAR_UNDEAD', 'T8_2H_HALBERD',
                'T8_2H_FIRESTAFF_HELL', 'T8_2H_ICECRYSTAL_UNDEAD', 'T8_2H_ICEGAUNTLETS_HELL', 'T8_2H_ENIGMATICORB_MORGANA',
                'T8_2H_ARCANESTAFF_HELL', 'T8_2H_HOLYSTAFF_HELL', 'T8_2H_NATURESTAFF_KEEPER', 'T8_2H_CURSEDSTAFF_MORGANA',
                'T8_2H_REPEATINGCROSSBOW_UNDEAD', 'T8_2H_CROSSBOWLARGE_MORGANA', 'T8_2H_BOW_HELL', 'T8_2H_LONGBOW', 'T8_2H_FROSTSTAFF',
                'T8_MAIN_RAPIER_MORGANA', 'T8_2H_DUALSWORD', 'T8_2H_DUALAXE_KEEPER', 'T8_2H_FLAIL', 'T8_2H_MACE', 'T8_2H_HOLYSTAFF',
                'T8_2H_NATURESTAFF_HELL', 'T8_2H_HALBERD_MORGANA', 'T8_2H_SCYTHE_HELL', 'T8_2H_ENIGMATICSTAFF', 'T8_2H_CROSSBOW_CANNON_AVALON',
                'T8_MAIN_SPEAR_LANCE_AVALON', 'T8_2H_ARCANE_RINGPAIR_AVALON', 'T8_2H_WILDSTAFF', 'T8_2H_BOW_AVALON', 'T8_MAIN_SCIMITAR_MORGANA', 'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Helmet', (faker, i, data) => {
    return {
        name: ['Mercenary Hood', 'Knight Helmet', 'Judicator Helmet', 'Royal Hood', 'Scholar Cowl',
            'Assassin Hood', 'Soldier Helmet', 'Stalker Hood', 'Guardian Helmet', 'wlasny'][i],
        item_name: ['T8_HEAD_LEATHER_SET1', 'T8_HEAD_PLATE_SET2', 'T8_HEAD_PLATE_KEEPER', 'T8_HEAD_LEATHER_ROYAL', 'T8_HEAD_CLOTH_SET1',
            'T8_HEAD_LEATHER_SET3', 'T8_HEAD_PLATE_SET1', 'T8_HEAD_LEATHER_MORGANA', 'T8_HEAD_PLATE_SET3', 'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Armor', (faker, i, data) => {
    return {
        name: ['Cleric Robe', 'Scholar Robe', 'Knight Armor', 'Guardian Armor', 'Demon Armor',
            'Judicator Armor', 'Soldier Armor', 'Specter Jacket', 'Assassin Jacket', 'Hellion Jacket',
            'Armor of Valor', 'Robe of Purity', 'Mage Robe', 'wlasny'][i],
        item_name: ['T8_ARMOR_CLOTH_SET2', 'T8_ARMOR_CLOTH_SET1', 'T8_ARMOR_PLATE_SET2', 'T8_ARMOR_PLATE_SET3', 'T8_ARMOR_PLATE_HELL',
            'T8_ARMOR_PLATE_KEEPER', 'T8_ARMOR_PLATE_SET1', 'T8_ARMOR_LEATHER_UNDEAD', 'T8_ARMOR_LEATHER_SET3', 'T8_ARMOR_LEATHER_HELL',
            'T8_ARMOR_PLATE_AVALON', 'T8_ARMOR_CLOTH_AVALON', 'T8_ARMOR_CLOTH_SET3', 'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Boot', (faker, i, data) => {
    return {
        name: ['Scholar Sandals', 'Hunter Shoes', 'Guardian Boots', 'Royal Sandals', 'Mage Sandals',
            'Demon Boots', 'Judicator Boots', 'wlasny'][i],
        item_name: ['T8_SHOES_CLOTH_SET1', 'T8_SHOES_LEATHER_SET2', 'T8_SHOES_PLATE_SET3', 'T8_SHOES_CLOTH_ROYAL', 'T8_SHOES_CLOTH_SET3',
            'T8_SHOES_PLATE_HELL', 'T8_SHOES_PLATE_KEEPER', 'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Cape', (faker, i, data) => {
    return {
        name: ['Lymhurst Cape', 'Fort Sterling Cape', 'Martlock Cape', 'Keeper Cape', 'Morgana Cape',
            'wlasny'][i],
        item_name: ['T8_CAPEITEM_FW_LYMHURST', 'T8_CAPEITEM_FW_FORTSTERLING', 'T8_CAPEITEM_FW_MARTLOCK', 'T8_CAPEITEM_KEEPER', 'T8_CAPEITEM_MORGANA',
            'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Offhand', (faker, i, data) => {
    return {
        name: ['Taproot', 'Muisak', 'Celestial Censer', 'Facebreaker', 'Sacred Scepter',
            'wlasny'][i],
        item_name: ['T8_OFF_TOTEM_KEEPER', 'T8_OFF_DEMONSKULL_HELL', 'T8_OFF_CENSER_AVALON', 'T8_OFF_SPIKEDSHIELD_MORGANA', 'T8_OFF_TALISMAN_AVALON',
            'T8_TRASH'][i],
    }
})

Factory.blueprint('App/Models/Permission', (faker, i, data) => {
    return {
        name: ['Admin'][i],
        weight: [1][i]
    }
})

Factory.blueprint('App/Models/Rank', (faker, i, data) => {
    return {
        user: [2][i],
        rank: [1][i],
    }
})


Factory.blueprint('App/Models/PermAttribute', (faker, i, data) => {
    return {
        name: ['CAN_SEE_HOMEPAGE', 'CAN_SEE_BLACKLIST', 'CAN_SEE_REGEAR', 'CAN_SEE_ZVZ', 'CAN_SEE_RDAY_DECLARATIONS',
            'CAN_SEE_RDAY_SETS', 'CAN_DO_REGEAR_APPLY', 'CAN_DO_REGEAR_REJECT'][i],
        type: ['canSee', 'canSee', 'canSee', 'canSee', 'canSee', 'canSee', 'canDo', 'canDo'][i],
        value: [1, 1, 1, 1, 1, 1, 1, 1][i],
        rank: [1, 1, 1, 1, 1, 1, 1, 1][i],
    }
})

Factory.blueprint('App/Models/Inventory', (faker, i, data) => {
    return {
        item:
            ['Camlann Mace', 'Grovekeeper', 'Soulscythe', 'Galantine pair', 'Halberd',
                'Brimstone Staff', 'Permafrost', 'Icicle Staff', 'Malevolent Locus', 'Occult Staff',
                'Fallen Staff', 'Rampant Staff', 'Damnation Staff', 'Weeping Reapeter', 'Siegebow',
                'Wailing Bow', 'Longbow', 'Great Frost', 'Bloodletter', 'Dual Swords', 'Bear Paws',
                'Morning Star', 'Heavy Mace', 'Great Holy Staff', 'Blight Staff', 'Carrioncaller',
                'Infernal Scythe', 'Enigmatic Staff', 'Energy Shaper', 'Daybreker', 'Evensong',
                'Wild Staff', 'Mistpiercer', 'Clarent', 'Mercenary Hood', 'Knight Helmet',
                'Judicator Helmet', 'Royal Hood', 'Scholar Cowl',
                'Assassin Hood', 'Soldier Helmet', 'Stalker Hood', 'Guardian Helmet',
                'Cleric Robe', 'Scholar Robe', 'Knight Armor', 'Guardian Armor', 'Demon Armor',
                'Judicator Armor', 'Soldier Armor', 'Specter Jacket', 'Assassin Jacket', 'Hellion Jacket',
                'Armor of Valor', 'Robe of Purity', 'Mage Robe', 'Scholar Sandals', 'Hunter Shoes',
                'Guardian Boots', 'Royal Sandals', 'Mage Sandals', 'Demon Boots', 'Judicator Boots',
                'Lymhurst Cape', 'Fort Sterling Cape', 'Martlock Cape', 'Keeper Cape', 'Morgana Cape',
                'Taproot', 'Muisak', 'Celestial Censer', 'Facebreaker', 'Sacred Scepter'][i],
        item_name:
            ['T8_2H_MACE_MORGANA', 'T8_2H_RAM_KEEPER', 'T8_2H_TWINSCYTHE_HELL', 'T8_2H_DUALSCIMITAR_UNDEAD', 'T8_2H_HALBERD',
                'T8_2H_FIRESTAFF_HELL', 'T8_2H_ICECRYSTAL_UNDEAD', 'T8_2H_ICEGAUNTLETS_HELL', 'T8_2H_ENIGMATICORB_MORGANA',
                'T8_2H_ARCANESTAFF_HELL', 'T8_2H_HOLYSTAFF_HELL', 'T8_2H_NATURESTAFF_KEEPER', 'T8_2H_CURSEDSTAFF_MORGANA',
                'T8_2H_REPEATINGCROSSBOW_UNDEAD', 'T8_2H_CROSSBOWLARGE_MORGANA', 'T8_2H_BOW_HELL', 'T8_2H_LONGBOW', 'T8_2H_FROSTSTAFF',
                'T8_MAIN_RAPIER_MORGANA', 'T8_2H_DUALSWORD', 'T8_2H_DUALAXE_KEEPER', 'T8_2H_FLAIL', 'T8_2H_MACE', 'T8_2H_HOLYSTAFF',
                'T8_2H_NATURESTAFF_HELL', 'T8_2H_HALBERD_MORGANA', 'T8_2H_SCYTHE_HELL', 'T8_2H_ENIGMATICSTAFF', 'T8_2H_CROSSBOW_CANNON_AVALON',
                'T8_MAIN_SPEAR_LANCE_AVALON', 'T8_2H_ARCANE_RINGPAIR_AVALON', 'T8_2H_WILDSTAFF', 'T8_2H_BOW_AVALON', 'T8_MAIN_SCIMITAR_MORGANA',
                'T8_HEAD_LEATHER_SET1', 'T8_HEAD_PLATE_SET2', 'T8_HEAD_PLATE_KEEPER', 'T8_HEAD_LEATHER_ROYAL', 'T8_HEAD_CLOTH_SET1',
                'T8_HEAD_LEATHER_SET3', 'T8_HEAD_PLATE_SET1', 'T8_HEAD_LEATHER_MORGANA', 'T8_HEAD_PLATE_SET3',
                'T8_ARMOR_CLOTH_SET2', 'T8_ARMOR_CLOTH_SET1', 'T8_ARMOR_PLATE_SET2', 'T8_ARMOR_PLATE_SET3', 'T8_ARMOR_PLATE_HELL',
                'T8_ARMOR_PLATE_KEEPER', 'T8_ARMOR_PLATE_SET1', 'T8_ARMOR_LEATHER_UNDEAD', 'T8_ARMOR_LEATHER_SET3', 'T8_ARMOR_LEATHER_HELL',
                'T8_ARMOR_PLATE_AVALON', 'T8_ARMOR_CLOTH_AVALON', 'T8_ARMOR_CLOTH_SET3', 'T8_SHOES_CLOTH_SET1', 'T8_SHOES_LEATHER_SET2',
                'T8_SHOES_PLATE_SET3', 'T8_SHOES_CLOTH_ROYAL', 'T8_SHOES_CLOTH_SET3', 'T8_SHOES_PLATE_HELL', 'T8_SHOES_PLATE_KEEPER',
                'T8_CAPEITEM_FW_LYMHURST', 'T8_CAPEITEM_FW_FORTSTERLING', 'T8_CAPEITEM_FW_MARTLOCK', 'T8_CAPEITEM_KEEPER', 'T8_CAPEITEM_MORGANA',
                'T8_OFF_TOTEM_KEEPER', 'T8_OFF_DEMONSKULL_HELL', 'T8_OFF_CENSER_AVALON', 'T8_OFF_SPIKEDSHIELD_MORGANA', 'T8_OFF_TALISMAN_AVALON'][i],
    }
})

