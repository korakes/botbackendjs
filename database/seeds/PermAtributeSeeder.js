'use strict'

/*
|--------------------------------------------------------------------------
| PermAtributeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class PermAtributeSeeder {
  async run() {
    const PermAttribute = await Factory
      .model('App/Models/PermAttribute')
      .createMany(8)
  }
}

module.exports = PermAtributeSeeder
