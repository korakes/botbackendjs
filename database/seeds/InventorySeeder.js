'use strict'

/*
|--------------------------------------------------------------------------
| InventorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class InventorySeeder {
  async run() {
    const Inventory = await Factory
      .model('App/Models/Inventory')
      .createMany(73)
  }
}

module.exports = InventorySeeder
