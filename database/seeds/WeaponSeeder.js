'use strict'

/*
|--------------------------------------------------------------------------
| WeaponSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class WeaponSeeder {
  async run() {
    const weapon = await Factory
      .model('App/Models/Weapon')
      .createMany(35)
  }
}

module.exports = WeaponSeeder
