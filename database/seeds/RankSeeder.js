'use strict'

const Rank = require('../../app/Models/Rank')

/*
|--------------------------------------------------------------------------
| RankSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class RankSeeder {
  async run () {
    const Rank = await Factory
      .model('App/Models/Rank')
      .createMany(1)

  }
}

module.exports = RankSeeder
