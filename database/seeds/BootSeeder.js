'use strict'

/*
|--------------------------------------------------------------------------
| BootSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class BootSeeder {
  async run () {
    const boot = await Factory
    .model('App/Models/Boot')
    .createMany(8)
  }
}

module.exports = BootSeeder
