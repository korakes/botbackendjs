'use strict'

/*
|--------------------------------------------------------------------------
| ArmorSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class OffhandSeeder {
  async run () {
    const armor = await Factory
    .model('App/Models/Offhand')
    .createMany(6)
  }
}

module.exports = OffhandSeeder
