'use strict'

/*
|--------------------------------------------------------------------------
| CapeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class CapeSeeder {
  async run () {
    const cape = await Factory
    .model('App/Models/Cape')
    .createMany(6)
  }
}

module.exports = CapeSeeder
