'use strict'

/*
|--------------------------------------------------------------------------
| ArmorSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ArmorSeeder {
  async run () {
    const armor = await Factory
    .model('App/Models/Armor')
    .createMany(14)
  }
}

module.exports = ArmorSeeder
