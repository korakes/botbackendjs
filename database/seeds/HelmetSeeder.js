'use strict'

/*
|--------------------------------------------------------------------------
| HelmetSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class HelmetSeeder {
  async run () {
    const helmet = await Factory
    .model('App/Models/Helmet')
    .createMany(10)
  }
}

module.exports = HelmetSeeder
