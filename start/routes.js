'use strict'

const ZvzController = require('../app/Controllers/Http/ZvzController');

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    Route.get('/recomendations', 'PlayerController.index')
    Route.post('/recomendation/', 'PlayerController.addrecomendation')
    Route.get('/blacklist/', 'BlacklistController.index')
    Route.get('/blacklist/check/', 'BlacklistController.details')
    Route.post('/blacklist/', 'BlacklistController.addBlacklist')
    Route.delete('/blacklist/remove/', 'BlacklistController.delete');
    Route.get('/fine/', 'FineController.index')
    Route.get('/fine/check', 'FineController.details')
    Route.post('/fine/', 'FineController.addFine')
    Route.put('/fine/paid/', 'FineController.changeStatus')
    Route.get('/rday/count', 'RdayController.getCount')
    Route.get('/members', 'MemberController.index')
    // Route.delete('/members/remove_id', 'MemberController.removeMember2')
    Route.post('/members/add', 'MemberController.addMember')
    Route.get('/members/find', 'MemberController.getDiscord')
    Route.get('/members/find_ing', 'MemberController.getIng')
}).prefix('api');

Route.group(() => {
    Route.post('/login', 'AuthController.login')
    Route.post('/register', 'AuthController.register').middleware(['auth'])
    Route.get('/checktoken', 'AuthController.checkToken').middleware(['auth'])

    Route.get('/members', 'MemberController.index')
    Route.post('/members/add', 'MemberController.addMember').middleware(['auth'])
    Route.delete('/members/remove', 'MemberController.removeMember').middleware(['auth'])
    Route.post('/member/obligated', 'MemberController.memberObligated')
    Route.get('/members/notobligated', 'MemberController.findNotObligated')
    
    Route.get('/blacklist/', 'BlacklistController.index').middleware(['auth'])
    Route.delete('/blacklist/remove', 'BlacklistController.delete').middleware(['auth'])

    Route.get('/zvz/players', 'ZvzController.getPlayers').middleware(['auth'])
    Route.get('/zvz/zvzs', 'ZvzController.getZvzs').middleware(['auth'])
    Route.get('/zvz/last', 'ZvzController.getLastZvz').middleware(['auth'])
    Route.get('/zvz/lastplayerzvz', 'ZvzController.getLastPlayerZvzs').middleware(['auth'])
    Route.get('/zvz/findbydate', 'ZvzController.findZvzByBetweenDate').middleware(['auth'])
    Route.get('/zvz/findplayerzvz', 'ZvzController.getPlayerZvzs').middleware(['auth'])
    Route.get('/zvz/findplayerzvzbyDate', 'ZvzController.getPlayerZvzsByDate').middleware(['auth'])
    Route.get('/zvz/findplayerlastweekzvzs', 'ZvzController.getPlayerLastWeekZvzs').middleware(['auth'])
    Route.get('/zvz/summary', 'ZvzController.summaryZvzs').middleware(['auth'])
    Route.get('/zvz/summarybydate', 'ZvzController.summaryZvzsByDate').middleware(['auth'])
    Route.patch('/zvz/update', 'ZvzController.updateZvzDesc').middleware(['auth'])
    Route.post('/zvz/add', 'ZvzController.storeZvz').middleware(['auth'])
    Route.post('/zvz/addPlayer', 'ZvzController.addPlayerToZvz').middleware(['auth'])
    Route.delete('/zvz/removePlayer', 'ZvzController.removePlayer').middleware(['auth'])
    Route.delete('/zvz/removeZvz', 'ZvzController.removeZvz').middleware(['auth'])

    Route.get('/rdays', 'RdayController.index').middleware(['auth'])
    Route.get('/rday/shamelist', 'RdayController.getShameRdayList').middleware(['auth'])
    Route.get('/rday/check', 'RdayController.checkRday').middleware(['auth'])
    Route.post('/rday/open', 'RdayController.openRday').middleware(['auth'])
    Route.post('/rday/close', 'RdayController.closeRday').middleware(['auth'])
    Route.get('/rday/declarations', 'RdayController.declarations').middleware(['auth'])
    Route.get('/rday/declarationssimple', 'RdayController.declarationsSimple')
    Route.get('/rday/declaration/findSet', 'RdayController.findSetByName').middleware(['auth'])
    Route.post('/rday/declaration', 'RdayController.addDeclaration').middleware(['auth'])
    Route.delete('/rday/declaration', 'RdayController.deleteDeclaration').middleware(['auth'])
    Route.get('/rday/sets', 'RdayController.sets').middleware(['auth'])
    Route.delete('/rday/set', 'RdayController.removeSet').middleware(['auth'])
    Route.patch('/rday/set/edit', 'RdayController.editSet').middleware(['auth'])
    Route.get('/rday/sets/items', 'RdayController.items').middleware(['auth'])
    Route.post('/rday/sets/add', 'RdayController.addNewSetGroup').middleware(['auth'])
    Route.post('/rday/item/weapon', 'RdayController.addWeapon').middleware(['auth'])
    Route.post('/rday/item/helmet', 'RdayController.addHelmet').middleware(['auth'])
    Route.post('/rday/item/armor', 'RdayController.addArmor').middleware(['auth'])
    Route.post('/rday/item/boots', 'RdayController.addBoots').middleware(['auth'])
    Route.post('/rday/item/offhand', 'RdayController.addOffhand').middleware(['auth'])
    Route.post('/rday/item/cape', 'RdayController.addCape').middleware(['auth'])

    Route.get('/inventory', 'InventoryController.index').middleware(['auth', 'permission:CAN_SEE_ARMORY'])
    Route.get('/inventory/amount', 'InventoryController.findSmallAmount').middleware(['auth', 'permission:CAN_SEE_ARMORY'])
    Route.post('/inventory/item/add', 'InventoryController.addItem').middleware(['auth', 'permission:CAN_CHANGE_ARMORY_VALUES'])
    Route.patch('/inventory/item/increase', 'InventoryController.increaseAmount').middleware(['auth', 'permission:CAN_CHANGE_ARMORY_VALUES'])
    Route.patch('/inventory/substractset', 'InventoryController.subtractSet').middleware(['auth', 'permission:CAN_CHANGE_ARMORY_VALUES'])

    Route.get('/regears', 'RegearController.index').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo', 'RegearController.regearsToDo').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo/tanks', 'RegearController.findTanksRegear').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo/mdps', 'RegearController.findMdpsRegear').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo/rdps', 'RegearController.findRdpsRegear').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo/healers', 'RegearController.findHealersRegear').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.get('/regear/todo/supports', 'RegearController.findSupportsRegear').middleware(['auth','permission:CAN_SEE_REGEAR'])
    Route.post('/regear/add', 'RegearController.addRegear').middleware(['auth', 'permission:CAN_ADD_NEW_REGEAR'])
    Route.patch('/regear/apply', 'RegearController.applyRegear').middleware(['auth','permission:CAN_DO_REGEAR_APPLY'])
    Route.patch('/regear/reject', 'RegearController.rejectRegear').middleware(['auth','permission:CAN_DO_REGEAR_REJECT'])

    Route.post('/permission/add', 'PermissionsController.addPermission').middleware(['auth', 'permission:CAN_MANAGEMENT_PERMISSIONS'])
    Route.post('/permission/rank/add', 'PermissionsController.addRank').middleware(['auth', 'permission:CAN_MANAGEMENT_PERMISSIONS'])
    Route.post('/permission/attr/add', 'PermissionsController.addPermAttribute').middleware(['auth', 'permission:CAN_MANAGEMENT_PERMISSIONS'])
    Route.patch('/permission/rank/change', 'PermissionsController.changeUserRank').middleware(['auth', 'permission:CAN_MANAGEMENT_PERMISSIONS'])

    Route.get('/permissionsattr', 'PermissionsController.indexAttr').middleware(['auth', 'permission:CAN_SEE_PERMISSIONS'])
    Route.get('/permissions', 'PermissionsController.indexPermissions').middleware(['auth', 'permission:CAN_SEE_PERMISSIONS'])
    Route.get('/ranks', 'PermissionsController.indexRanks').middleware(['auth', 'permission:CAN_SEE_PERMISSIONS'])
    Route.get('/users', 'PermissionsController.indexUsers').middleware(['auth', 'permission:CAN_SEE_USERS_LIST'])

}).prefix('api/auth')

Route.on('/').render('welcome')
