'use strict'

const Response = require('@adonisjs/framework/src/Response')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User')

class Permission {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, auth, response }, next, properties) {
    // call next to advance the request
    const findUser = await auth.getUser()
    const getPermissions = await User.query().with('Ranks', (builder) => {
      builder.with('Permissions', (builder) => {
        builder.with('PermAtr')
      })
    }).where('id', findUser.id).firstOrFail()
    const user = getPermissions.toJSON()
    const userPerms = user.Ranks.Permissions.PermAtr

    if(userPerms.filter((element, index) => { return (element.name === properties[0] && element.value === 1) }).length != 0) {
      
    } else {
      response.status(403).send({message: 'Nie masz uprawnień aby przeglądać tą stronę. ' + properties[0] + ' error.'})
    }
    await next()
  }
}

module.exports = Permission
