'use strict'
const Permission = use('App/Models/Permission')
const Rank = use('App/Models/Rank')
const PermAttribute = use('App/Models/PermAttribute')
const User = use('App/Models/User')

class PermissionsController {
    async indexAttr() {
        const attr = await PermAttribute.all();
        return attr.toJSON();
    }

    async indexUsers() {
        const user = await User.query().with('Ranks', (builder) => {
            builder.with('Permissions', (builder) => {
                builder.with('PermAtr')
            })
        }).fetch()
        return user.toJSON();
    }

    async indexRanks() {
        const rank = await Rank.all();
        return rank.toJSON();
    }

    async indexPermissions() {
        const permissions = await Permission.query().with('PermAtr').fetch();
        return permissions.toJSON();
    }

    async addPermAttribute({ request }) {
        //POST
        //name, type[enum], value, rank

        const newAtr = new PermAttribute()

        newAtr.name = request.input('name')
        newAtr.type = request.input('type')
        newAtr.value = request.input('value')
        newAtr.rank = request.input('rank')

        newAtr.save()

        return newAtr
    }

    async addPermission({ request }) {
        //POST
        //name, weight = 100

        const addPerm = new Permission()

        addPerm.name = request.input('name')
        if (request.input('weight')) addPerm.weight = request.input('weight')

        addPerm.save()

        return addPerm
    }

    async addRank({ request }) {
        //POST
        //user, rank

        const rank = new Rank()

        rank.user = request.input('user')
        rank.rank = request.input('rank')

        rank.save()

        return rank
    }

    async changeUserRank({request}) {
        //PATCH
        //userid, rankid

        const findUser = await Rank.query().where('user', request.input('userid')).firstOrFail()
        const findRank = await Permission.query().where('id', request.input('rankid')).firstOrFail()

        findUser.rank = findRank.id

        findUser.save()

        return findUser
    }
}

module.exports = PermissionsController
