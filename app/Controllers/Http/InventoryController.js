'use strict'

const Inventory = use('App/Models/Inventory')

class InventoryController {
    async index() {
        const inventory = await Inventory.all();
        return inventory.toJSON();
    }

    async addItem({request}) {
        //POST
        //item, item_name

        const addItem = new Inventory()

        addItem.item = request.input('item')
        addItem.item_name = request.input('item_name')

        await addItem.save()

        return addItem
    }

    async increaseAmount({request}) {
        //PATCH
        //id, count

        const increase = await Inventory.query().where('id', request.input('id')).firstOrFail()

        increase.count = parseInt(increase.count) + parseInt(request.input('count'))

        await increase.save()

        return increase
    }

    async subtractSet({ request, response, session }) {
        //PATCH
        //weapon, helmet, armor, boots, offhand, cape

        let res = []
    
        const findWeapon = await Inventory.query().where('item', request.input('weapon')).firstOrFail()
        const findHelmet = await Inventory.query().where('item', request.input('helmet')).firstOrFail()
        const findArmor = await Inventory.query().where('item', request.input('armor')).firstOrFail()
        const findBoots = await Inventory.query().where('item', request.input('boots')).firstOrFail()
        const findCape = await Inventory.query().where('item', request.input('cape')).firstOrFail()
        if(request.input('offhand')) {
            const findOffhand = await Inventory.query().where('item', request.input('offhand')).firstOrFail()
            findOffhand.count = findOffhand.count--
            findOffhand.save()

            res.push(findOffhand)
        }

        findWeapon.count = findWeapon.count--
        findHelmet.count = findHelmet.count--
        findArmor.count = findArmor.count--
        findBoots.count = findBoots.count--
        findCape.count = findCape.count--

        await findWeapon.save()
        res.push(findWeapon)
        await findHelmet.save()
        res.push(findHelmet)
        await findArmor.save()
        res.push(findArmor)
        await findBoots.save()
        res.push(findBoots)
        await findCape.save()
        res.push(findCape)
        
        return response.json(res)
    }

    async findSmallAmount() {
        const smallAmount = await Inventory.query().where('count', '<=', 10).fetch()

        return smallAmount
    }
}

module.exports = InventoryController
