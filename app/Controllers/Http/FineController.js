'use strict'

const Fine = use('App/Models/Fine')

class FineController {
    async index() {
        const fines = await Fine.all();
        return fines
    }

    async details({ request, response, session }) {
        const details = await Fine.query().where('status', 0).where('nick', request.input('nick')).fetch()

        return details
    }

    async addFine({ request, response, session }) {
        const fine = new Fine();

        fine.nick = request.input('nick')
        fine.value = request.input('value')
        fine.reason = request.input('reason')
        fine.status = request.input('status')
        await fine.save();

        return fine;
    }

    async changeStatus({ request, response, session })  {
        const fine = await Fine.findByOrFail('id', request.input('id'))

        fine.status = 1
        await fine.save()

        return fine;
    }
}

module.exports = FineController
