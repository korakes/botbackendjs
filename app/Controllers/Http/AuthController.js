'use strict'
const User = use('App/Models/User')

class AuthController {
    async register({ request, auth, response }) {
        let user = await User.create(request.all())

        let token = await auth.generate(user)
        Object.assign(user, token)

        return response.json(user)
    }

    async login({ request, auth, response }) {
        let { login, password } = request.all();

        try {
            if (await auth.attempt(login, password)) {
                let user = await User.query().with('Ranks', (builder) => {
                    builder.with('Permissions', (builder) => {
                        builder.with('PermAtr')
                    })
                }).where('login', login).first()
                let token = await auth.generate(user)

                Object.assign(user, token)
                return response.status(202).json(user)
            }
        }
        catch (e) {
            console.log(e)
            return response.json({ message: 'Nie znaleziono konta.' })
        }
    }

    async checkToken({ request, auth, response }) {
        try {
            const checkUser = await auth.getUser()
            const user = User.query().with('Ranks', (builder) => {
                builder.with('Permissions', (builder) => {
                    builder.with('PermAtr')
                })
            }).where('id', checkUser.id).firstOrFail()

            return user

        } catch (error) {
            response.status(403).send('Missing or invalid jwt token')
        }
    }
}

module.exports = AuthController
