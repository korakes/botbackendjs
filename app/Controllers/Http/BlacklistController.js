'use strict'

const Blacklist = use('App/Models/Blacklist')

class BlacklistController {

    async index() {
        const blacklist = await Blacklist.all();
        return blacklist.toJSON();
    }

    async addBlacklist({ request, response, session }) {
        const blacklist = new Blacklist();

        blacklist.nick = request.input('nick')
        blacklist.discordid = request.input('discordid')
        blacklist.reason = request.input('reason')
        await blacklist.save();

        return blacklist;
    }

    async details({ request, response, session }) {
        const check = await Blacklist.findByOrFail('nick', request.input('nick'))

        return check
    }

    async delete({ request, session, response }) {
        const blacklist = await Blacklist.findByOrFail('nick', request.input('nick'));

        await blacklist.delete()

        return blacklist;
    }
}

module.exports = BlacklistController
