'use strict'

const Recomendation = use('App/Models/Recomendation')


class recomendationController {
    async index() {
        const recomendations = await Recomendation.all();
        return recomendations.toJSON();
    }

    async addrecomendation({ request, response, session }) {
        const recomendation = new Recomendation();

        recomendation.name = request.input('name');
        recomendation.referrer = request.input('referrer');

        await recomendation.save();

        return recomendation;
    }

    async removerecomendation({ request, response, session }) {
        const recomendation = await Recomendation.findByOrFail('name', request.input('name'));

        await recomendation.delete()

        return recomendation;
    }
}

module.exports = recomendationController
