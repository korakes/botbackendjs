'use strict'

const Regear = use('App/Models/Regear')
const Member = use('App/Models/Member')
const SGroup = use('App/Models/SGroup')
const Inventory = use('App/Models/Inventory')

class RegearController {
    async index() {
        const regear = await Regear.all();
        return regear.toJSON();
    }

    async findTanksRegear() {
        const tanks = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).whereHas('Set', (builder) => {
            builder.where('type', 'Tank')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return tanks;
    }

    async findRdpsRegear() {
        const rdps = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).whereHas('Set', (builder) => {
            builder.where('type', 'Rdps')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return rdps;
    }

    async findMdpsRegear() {
        const mdps = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).whereHas('Set', (builder) => {
            builder.where('type', 'Mdps')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return mdps;
    }

    async findSupportsRegear() {
        const supports = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).whereHas('Set', (builder) => {
            builder.where('type', 'Support')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return supports;
    }

    async findHealersRegear() {
        const healers = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).whereHas('Set', (builder) => {
            builder.where('type', 'Healer')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return healers;
    }

    async findRejected() {
        const rejected = await Regear.query().where('isRejected', 1).fetch()

        return rejected
    }


    async regearsToDo() {
        const todo = await Regear.query().with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).where('isDone', 0).where('isRejected', 0).fetch()

        return todo
    }

    async addRegear({ request }) {
        //POST
        //discord_id, slug, url

        const regear = new Regear()

        const findMember = await Member.query().where('discord_id', request.input('discord_id')).firstOrFail()
        const findSet = await SGroup.query().where('slug', request.input('slug')).firstOrFail()

        regear.user = findMember.id
        regear.zestaw = findSet.id
        regear.url = request.input('url')

        await regear.save()

        return regear

    }

    async applyRegear({ request }) {
        //PATCH
        //id

        const findRegear = await Regear.query().with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).where('id', request.input('id')).firstOrFail()

        const regear = findRegear.toJSON()

        const findWeapon = await Inventory.query().where('item', regear.Set.Weapon.name).firstOrFail()
        const findHelmet = await Inventory.query().where('item', regear.Set.Helmet.name).firstOrFail()
        const findArmor = await Inventory.query().where('item', regear.Set.Armor.name).firstOrFail()
        const findBoots = await Inventory.query().where('item', regear.Set.Boots.name).firstOrFail()
        const findCape = await Inventory.query().where('item', regear.Set.Cape.name).firstOrFail()

        if(regear.Set.Offhand) {
            const findOffhand = await Inventory.query().where('item', regear.Set.Offhand.name).firstOrFail()
            findOffhand.count = --findOffhand.count
            await findOffhand.save()
        }

        findWeapon.count = --findWeapon.count
        findHelmet.count = --findHelmet.count
        findArmor.count = --findArmor.count
        findBoots.count = --findBoots.count
        findCape.count = --findCape.count
        findRegear.isDone = 1

        await findWeapon.save()
        await findHelmet.save()
        await findArmor.save()
        await findBoots.save()
        await findCape.save()
        await findRegear.save()

        return findRegear
    }

    async rejectRegear({ request }) {
        //PATCH
        //id
        const reject = await Regear.query().where('id', request.input('id')).update({ isRejected: 1 })

        return reject
    }

}

module.exports = RegearController
