'use strict'

const ZvzPlayer = require("../../Models/ZvzPlayer");

const Member = use('App/Models/Member')
const RdayDeclaration = use('App/Models/RdayDeclaration')

class memberController {
    async index() {
        const members = await Member.all();
        return members.toJSON();
    }

    async addMember({ request, response, session }) {
        const member = new Member();

        member.nick = request.input('nick');
        member.ing_key = request.input('ing_key');
        member.discord_id = request.input('discord_id');

        await member.save();

        return member;
    }

    async getDiscord({ request, response, session }) {
        const getDiscord = await Member.query().where('discord_id', request.input('discord_id')).fetch()

        return getDiscord
    }

    async getIng({ request, response, session }) {
        const getIng = await Member.query().where('ing_key', request.input('ing_key')).fetch()

        return getIng
    }

    async removeMember({ request, response, session }) {

        //DELETE
        // discord_id

        if(request.input('discord_id') == 'DeletedUser') return response.status(403).json({message: 'U cannot delete DeletedUser.'})
        const removeMember = await Member.findByOrFail('discord_id', request.input('discord_id'));
        const findDelUser = await Member.findByOrFail('discord_id', 'DeletedUser')
        const findMemberZvzs = await ZvzPlayer.query().where('user', removeMember.id).update({user: findDelUser.id})
        try {
            await RdayDeclaration.query().where('user', removeMember.id).delete()
        } catch {}

        await removeMember.delete()

        return removeMember;
    }
    
    async removeMember2({ request, response, session }) {
        const removeMember2 = await Member.findByOrFail('id', request.input('id'));

        await removeMember2.delete()

        return removeMember2;
    }

    async findNotObligated({ request, response, session }) {
        const find = await Member.query().where('isObligated', 0).fetch()

        return find
    }

    async memberObligated({ request, response, session }) {
        const obligated = await Member.query().where('id', request.input('id')).update({isObligated: 1})

        return obligated
    }
}

module.exports = memberController
