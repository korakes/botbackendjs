'use strict'

const { find } = require("@adonisjs/framework/src/Route/Store")

const RecordRday = use('App/Models/RecordRday')
const Rday = use('App/Models/Rday')
const SGroup = use('App/Models/SGroup')
const RdayDeclaration = use('App/Models/RdayDeclaration')
const Member = use('App/Models/Member')

const Weapon = use('App/Models/Weapon')
const Offhand = use('App/Models/Offhand')
const Helmet = use('App/Models/Helmet')
const Armor = use('App/Models/Armor')
const Boot = use('App/Models/Boot')
const Cape = use('App/Models/Cape')

const Database = use('Database')

class RdayController {

    async index() {
        const rdays = await Rday.all()

        return rdays
    }

    async declarations({ request, response }) {
        const findRday = await Rday.query().where('status', 1).first()
        if (!findRday) return response.json({ message: 'Zapisy na rday są zamknięte.' })

        const declarations = await RdayDeclaration.query().where('rday', findRday.toJSON().id).with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).fetch()

        let count = {
            tank: 0,
            mdps: 0,
            rdps: 0,
            healer: 0,
            support: 0,
            total: 0
        }

        let tankCheck = await RdayDeclaration.query().whereHas('Set', (builder) => {
            builder.where('type', 'tank')
        }).where('rday', findRday.toJSON().id).count('* as tank')
        count = { ...count, tank: tankCheck[0].tank }

        let mdpsCheck = await RdayDeclaration.query().whereHas('Set', (builder) => {
            builder.where('type', 'mdps')
        }).where('rday', findRday.toJSON().id).count('* as mdps')
        count = { ...count, mdps: mdpsCheck[0].mdps }

        let rdpsCheck = await RdayDeclaration.query().whereHas('Set', (builder) => {
            builder.where('type', 'rdps')
        }).where('rday', findRday.toJSON().id).count('* as rdps')
        count = { ...count, rdps: rdpsCheck[0].rdps }

        let healerCheck = await RdayDeclaration.query().whereHas('Set', (builder) => {
            builder.where('type', 'healer')
        }).where('rday', findRday.toJSON().id).count('* as healer')
        count = { ...count, healer: healerCheck[0].healer }

        let supportCheck = await RdayDeclaration.query().whereHas('Set', (builder) => {
            builder.where('type', 'support')
        }).where('rday', findRday.toJSON().id).count('* as support')
        count = { ...count, support: supportCheck[0].support }

        let totalGet = await RdayDeclaration.query().where('rday', findRday.toJSON().id).count('* as total')
        count = { ...count, total: totalGet[0].total }

        return { findRday, declarations, count }
    }

    async declarationsSimple({ request, response, session }) {
        const findRday = await Rday.query().where('status', 1).first()
        if (!findRday) return response.json({ message: 'Zapisy na rday są zamknięte.' })

        let decSimple = []

        let declarations = await RdayDeclaration.query().where('rday', findRday.toJSON().id).with('Member').with('Set', (builder) => {
            builder.with('Weapon')
            builder.with('Offhand')
            builder.with('Helmet')
            builder.with('Armor')
            builder.with('Boots')
            builder.with('Cape')
        }).fetch()
        let decDupa = declarations.toJSON();

        decDupa.map((element, index) => {
            decSimple = [...decSimple, {
                nick: element.Member.nick,
                weapon: element.Set.Weapon.name,
                armor: element.Set.Armor.name,
                offhand: element.Set.Offhand ? element.Set.Offhand.name : element.Set.Offhand,
                helmet: element.Set.Helmet.name,
                boots: element.Set.Boots.name,
                cape: element.Set.Cape.name
            }]
        })

        return decSimple
    }

    async getShameRdayList({ request, response }) {

        //GET

        const shameList = await Database.raw('select * from `members` where not exists (select * from `rday_declarations` where `members`.`id` = `rday_declarations`.`user`)')
        
        return shameList[0]

    }

    async findSetByName({ request, response, session }) {
        const find = await SGroup.query().where('slug', request.input('slug')).firstOrFail()

        return find
    }

    async addDeclaration({ request, response }) {

        //POST
        //discord_id, slug, roles []

        const findSet = await SGroup.query().where('slug', request.input('slug')).fetch()
        if (findSet.toJSON().length == 0) return response.json({ message: 'Nie znaleziono zestawu.' })

        if (request.input('roles').findIndex(element => element == findSet.toJSON()[0].permission_rank_id) < 0) return response.json({ message: 'Nie masz masz uprawnień, aby zapisać się na tą rolę.' })

        const findUser = await Member.query().where('discord_id', request.input('discord_id')).fetch()
        if (findUser.toJSON().length == 0) return response.json({ message: 'Nieznaleziono gracza o discordid ' + request.input('discord_id') })


        const findRday = await Rday.query().where('status', 1).fetch()
        if (findRday.toJSON().length == 0) return response.json({ message: 'Zapisy na reset day są zamknięte.' })

        const checkUser = await RdayDeclaration.query().where('user', findUser.toJSON()[0].id).where('rday', findRday.toJSON()[0].id).fetch()
        if (checkUser.toJSON().length != 0) return response.json({ message: 'Prawdopodobnie złożyłeś już deklaracje.' })


        const addDeclaration = new RdayDeclaration()

        addDeclaration.user = findUser.toJSON()[0].id
        addDeclaration.zestaw = findSet.toJSON()[0].id
        addDeclaration.rday = findRday.toJSON()[0].id

        await addDeclaration.save()

        return response.json({ message: 'Deklaracja na set ' + findSet.toJSON()[0].slug + ' została pomyślnie złożona.' })
    }

    async deleteDeclaration({ request, response }) {

        //DELETE
        //declarationId

        const removeDeclaration = await RdayDeclaration.findByOrFail('id', request.input('declarationId'))

        await removeDeclaration.delete()

        return removeDeclaration
    }

    async removeSet({ request, response }) {

        //DELETE
        //setId

        const removeSet = await SGroup.findByOrFail('id', request.input('setId'))

        await removeSet.delete()

        return removeSet
    }

    async editSet({ request, response }) {

        //PATCH
        //id, weapon, helmet, armor, boots, cape, offhand, slug, type, perrmission_rank_id

        const findSet = await SGroup.findByOrFail('id', request.input('id'))

        findSet.weapon = request.input('weapon')
        findSet.offhand = request.input('offhand')
        findSet.helmet = request.input('helmet')
        findSet.armor = request.input('armor')
        findSet.boots = request.input('boots')
        findSet.cape = request.input('cape')
        findSet.slug = request.input('slug')
        findSet.type = request.input('type')
        findSet.permission_rank_id = request.input('permission_rank_id')

        await findSet.save()

        const set = await SGroup.query().with('Cape').with('Weapon').with('Offhand').with('Armor').with('Helmet').with('Boots').where('id', findSet.id).fetch()

        return set.toJSON()[0]
    }

    async addNewSetGroup({ request, response }) {
        const addNewSet = new SGroup()

        addNewSet.weapon = request.input('weapon')
        addNewSet.offhand = request.input('offhand')
        addNewSet.helmet = request.input('helmet')
        addNewSet.armor = request.input('armor')
        addNewSet.boots = request.input('boots')
        addNewSet.cape = request.input('cape')
        addNewSet.slug = request.input('slug')
        addNewSet.type = request.input('type')
        addNewSet.permission_rank_id = request.input('permission_rank_id')

        await addNewSet.save()

        const set = await SGroup.query().where('id', addNewSet.id).with('Cape').with('Weapon').with('Offhand').with('Armor').with('Helmet').with('Boots').fetch()

        return set.toJSON()[0]
    }

    async sets({ request, response }) {
        const sets = await SGroup.query().with('Cape').with('Weapon').with('Offhand').with('Armor').with('Helmet').with('Boots').fetch()

        return sets
    }

    async items({ request, response }) {
        const weapons = await Weapon.all()
        const offhands = await Offhand.all()
        const helmets = await Helmet.all()
        const armors = await Armor.all()
        const boots = await Boot.all()
        const capes = await Cape.all()

        return ({ weapons: weapons, offhands: offhands, helmets: helmets, armors: armors, boots: boots, capes: capes })
    }

    async openRday({ request, response, session }) {

        //POST
        //name

        const openRday = new Rday()

        openRday.status = 1
        openRday.name = request.input('name')

        await openRday.save()

        return openRday
    }

    async closeRday({ request, response, session }) {
        const closeRday = await Rday.findByOrFail('status', 1)

        closeRday.status = 0

        await closeRday.save()

        return closeRday;
    }

    async checkRday({ request, response, session }) {
        try {
            const check = await Rday.findByOrFail('status', 1)

            return response.status(302).send()
        }
        catch (err) {
            if (err.status === 404)
                return response.status(200).send()
        }
    }

    async getCount() {
        let itemsCount = {
            weapons: {},
            helmets: {},
            armors: {},
            boots: {},
            capes: {},
        }
        const countWeapons = await RecordRday.query().select('weapon').count('* as total').groupBy('weapon')
        itemsCount = { ...itemsCount, weapons: countWeapons }
        const countHelmets = await RecordRday.query().select('helmet').count('* as total').groupBy('helmet')
        itemsCount = { ...itemsCount, helmets: countHelmets }
        const countArmors = await RecordRday.query().select('armor').count('* as total').groupBy('armor')
        itemsCount = { ...itemsCount, armors: countArmors }
        const countBoots = await RecordRday.query().select('boots').count('* as total').groupBy('boots')
        itemsCount = { ...itemsCount, boots: countBoots }
        const countCapes = await RecordRday.query().select('cape').count('* as total').groupBy('cape')
        itemsCount = { ...itemsCount, capes: countCapes }
        return itemsCount
    }

    async addWeapon({ request, response }) {
        const weapon = new Weapon()

        weapon.name = request.input('name')
        weapon.item_name = request.input('item_name')

        await weapon.save()

        return weapon
    }

    async addOffhand({ request, response }) {
        const offhand = new Offhand()

        offhand.name = request.input('name')
        offhand.item_name = request.input('item_name')

        await offhand.save()

        return offhand
    }

    async addHelmet({ request, response }) {
        const helmet = new Helmet()

        helmet.name = request.input('name')
        helmet.item_name = request.input('item_name')

        await helmet.save()

        return helmet
    }

    async addArmor({ request, response }) {
        const armor = new Armor()

        armor.name = request.input('name')
        armor.item_name = request.input('item_name')

        await armor.save()

        return armor
    }

    async addBoots({ request, response }) {
        const boots = new Boot()

        boots.name = request.input('name')
        boots.item_name = request.input('item_name')

        await boots.save()

        return boots
    }

    async addWCape({ request, response }) {
        const cape = new Cape()

        cape.name = request.input('name')
        cape.item_name = request.input('item_name')

        await cape.save()

        return cape
    }


}

module.exports = RdayController
