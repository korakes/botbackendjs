'use strict'

const Zvz = use('App/Models/Zvz')
const ZvzPlayer = use('App/Models/ZvzPlayer')
const Member = use('App/Models/Member')

class ZvzController {

    async getZvzs({ request, response }) {
        const fetch = await Zvz.query().withCount('zvzPlayers').fetch()

        return fetch
    }

    async getLastZvz({ request, response }) {
        const lastZvz = await Zvz.query().orderBy('created_at', 'desc').first()

        return lastZvz
    }

    async getPlayers({ request, response }) {
        const fetch = await Zvz.query().with('zvzPlayers', (builder) => {
            builder.with('Member')
        }).where('id', request.input('zvzid')).fetch()

        const players = fetch.toJSON()
        let preetyPlayers = []

        for (let element of players[0].zvzPlayers) {
            preetyPlayers.push(element.Member.nick)
        }

        return preetyPlayers
    }

    async storeZvz({ request, response }) {
        // POST
        // note

        const storeZvz = new Zvz()

        storeZvz.note = request.input('note')

        await storeZvz.save()

        return storeZvz
    }

    async addPlayerToZvz({ request, response }) {

        // POST
        // id, zvz_id

        const getMember = await Member.query().whereRaw('discord_id = :id OR ing_key = :id OR nick = :id', {id: request.input('id')}).firstOrFail()

        const addZvzPlayer = new ZvzPlayer()

        addZvzPlayer.zvz_id = request.input('zvz_id')
        addZvzPlayer.user = getMember.id

        await addZvzPlayer.save()
        
        const nickname = []

        nickname.push(getMember.nick)

        return nickname
    }

    async removePlayer({ request, response }) {
        
        //DELETE
        //player, zvzId

        const member = await Member.query().where('nick', request.input('player')).firstOrFail()
        const remove = await ZvzPlayer.query().where('zvz_id', request.input('zvzId')).andWhere('user', member.id).firstOrFail()

        await remove.delete()

        return remove
    }

    async findZvzByBetweenDate({ request, response }) {

        //GET
        //from, to
        const zvz = await Zvz.query().whereBetween('created_at', [request.input('from'), request.input('to')]).withCount('zvzPlayers').fetch()

        return zvz
    }

    async removeZvz({ request, response }) {

        //DELETE
        //zvzId

        const findZvz = await Zvz.query().where('id', request.input('zvzId')).firstOrFail()
        await ZvzPlayer.query().where('zvz_id', findZvz.id).delete()

        await findZvz.delete()

        return findZvz
    }

    async getPlayerZvzs({ request, response }) {

        //GET
        //userId

        const total = await ZvzPlayer.query().where('user', request.input('userId')).count('* as total')

        return total[0]
    }

    async summaryZvzs({ request, response }) {
        //GET

        const summary = await Member.query().whereNot('nick', 'DeletedUser').withCount('zvzPlayer').orderBy('zvzPlayer_count', 'desc').fetch()

        return summary
    }

    async summaryZvzsByDate({ request, response }) {
        //GET
        //from, to
        const summaryByDate = await Member.query().whereNot('nick', 'DeletedUser').withCount('zvzPlayer', (builder) => {
            builder.whereBetween('zvz_players.created_at', [request.input('from'), request.input('to')])
        }).orderBy('zvzPlayer_count', 'desc').fetch()

        return summaryByDate
    }

    async getLastPlayerZvzs({ request, response }) {

        //GET
        //userId

        const lastZvz = await Zvz.query().whereHas('zvzPlayers', (builder) => {
            builder.where('user', request.input('userId'))
        }).orderBy('created_at', 'desc').first()
        return lastZvz
    }

    async getPlayerZvzsByDate({ request, response }) {

        //GET
        //userId, from[DATE], to[DATE]
        
        const betweenDate = await ZvzPlayer.query().where('user', request.input('userId')).whereBetween('created_at', [request.input('from'), request.input('to')]).count('* as between')
    
        return betweenDate
    }

    async getPlayerLastWeekZvzs({ request, response }) {
        //GET
        //userId

        let date = new Date()
        const now = new Date()
        let newdate = date.setDate(date.getDate() - 7)

        const lastWeek = await ZvzPlayer.query().where('user', request.input('userId')).whereBetween('created_at', [date.toJSON(), now.toJSON()] ).count('* as total')
        newdate = date.setDate(date.getDate() - 7)
        const lastTwoWeeks = await ZvzPlayer.query().where('user', request.input('userId')).whereBetween('created_at', [date.toJSON(), now.toJSON()] ).count('* as total')
        date = new Date()
        let lastMonthDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let firstMonthDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const lastMonth = await ZvzPlayer.query().where('user', request.input('userId')).whereBetween('created_at', [firstMonthDay.toJSON(), lastMonthDay.toJSON()] ).count('* as total')
        
        return {lastWeek: lastWeek[0].total, lastTwoWeeks: lastTwoWeeks[0].total, lastMonth: lastMonth[0].total}
    }

    async updateZvzDesc({ request, response }) {

        //GET
        //zvzId, note
        
        const updateZvzDesc = await Zvz.query().where('id', request.input('zvzId')).firstOrFail()

        updateZvzDesc.note = request.input('note')

        updateZvzDesc.save()

        return updateZvzDesc
    }
}

module.exports = ZvzController
