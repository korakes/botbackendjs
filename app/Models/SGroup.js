'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SGroup extends Model {
    Weapon() {
        return this.belongsTo('App/Models/Weapon', 'weapon', 'id')
    }
    Offhand() {
        return this.belongsTo('App/Models/Offhand', 'offhand', 'id')
    }
    Helmet() {
        return this.belongsTo('App/Models/Helmet', 'helmet', 'id')
    }
    Armor() {
        return this.belongsTo('App/Models/Armor', 'armor', 'id')
    }
    Boots() {
        return this.belongsTo('App/Models/Boot', 'boots', 'id')
    }
    Cape() {
        return this.belongsTo('App/Models/Cape', 'cape', 'id')
    }
}

module.exports = SGroup
