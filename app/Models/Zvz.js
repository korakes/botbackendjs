'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Zvz extends Model {
    zvzPlayers() {
        return this.hasMany('App/Models/ZvzPlayer')
    }

    zvzPlayersWithUser() {
        return this.manyThrough('App/Models/ZvzPlayer', 'User')
    }

}

module.exports = Zvz
