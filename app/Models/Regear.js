'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Regear extends Model {
    Set() {
        return this.belongsTo('App/Models/SGroup', 'zestaw', 'id')
    }
    Member() {
        return this.belongsTo('App/Models/Member', 'user', 'id')
    }
}

module.exports = Regear
