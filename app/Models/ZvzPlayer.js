'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ZvzPlayer extends Model {
    Zvz() {
        return this.belongsTo('App/Models/Zvz', 'user', 'id')
    }

    Member() {
        return this.belongsTo('App/Models/Member', 'user', 'id')
    }
}

module.exports = ZvzPlayer
