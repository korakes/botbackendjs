'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Permission extends Model {
    PermAtr() {
        return this.hasMany('App/Models/PermAttribute', 'id', 'rank')
    }
}

module.exports = Permission
