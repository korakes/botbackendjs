'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Member extends Model {
    zvzPlayer() {
        return this.belongsTo('App/Models/ZvzPlayer', 'id', 'user')
    }
    rdayDeclaration() {
        return this.belongsTo('App/Models/RdayDeclaration')
    }
}

module.exports = Member
