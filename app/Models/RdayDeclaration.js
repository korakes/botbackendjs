'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RdayDeclaration extends Model {
    // table.integer('user').unsigned().references('id').inTable('users')
    // table.integer('set').unsigned().references('id').inTable('set_group')
    // table.integer('rday').unsigned().references('id').inTable('rdays')

    Member() {
        return this.belongsTo('App/Models/Member', 'user', 'id')
    }
    Rday() {
        return this.belongsTo('App/Models/Rday', 'rday', 'id')
    }
    Set() {
        return this.belongsTo('App/Models/SGroup', 'zestaw', 'id').orderBy('type', 'desc')
    }
}

module.exports = RdayDeclaration
