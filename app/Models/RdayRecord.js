'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RdayRecord extends Model {
    set() {
        return this.hasMany('App/Models/Set')
    }
}

module.exports = RdayRecord
