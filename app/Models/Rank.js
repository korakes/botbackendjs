'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Rank extends Model {
    Permissions() {
        return this.hasOne('App/Models/Permission', 'rank', 'id')
    }
}

module.exports = Rank
